import unittest

from main import WaterTowerSolution


class MyTestCase(unittest.TestCase):
    """
    We poured 2 cups to the top glass of the tower (which is indexed as (0, 0)).
    There is one cup of excess liquid.
    The glass indexed as (1, 0) and the glass indexed as (1, 1) will share the excess liquid equally, and each will get half cup of water.
    """

    def test_poured_with_2_cups_water(self):
        wt = WaterTowerSolution(2, 1, 1)
        result = wt.water_tower()
        self.assertEqual(result, 0.5)

    """
    We poured 1 cup to the top glass of the tower (which is indexed as (0, 0)).
     There will be no excess liquid so all the glasses under the top glass will remain empty.
    """

    def test_poured_only_1_cup_water(self):
        wt = WaterTowerSolution(1, 1, 1)
        result = wt.water_tower()
        self.assertEqual(result, 0)

    """
        We poured the number of cup water and all glass will be full of water
    """

    def test_poured_with_all_glasses_is_full(self):
        wt = WaterTowerSolution(11, 3, 6)
        result = wt.water_tower()
        self.assertEqual(result, 1)


if __name__ == '__main__':
    unittest.main()
