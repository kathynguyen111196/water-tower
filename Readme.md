##Dependency Requirement
- Please make sure you install these packages:
  - python>3.6
  - pytest
##How to use
###Execute command line
- To run the main function please use this command with 3 params like example command:

    ` python main.py 2 1 1`
  - First param will be the number of glasses of water you will use to poured
  - Second param will be the number of row
  - And third param will be the glass number
- You can use this command to get more information about params: `python main.py -h`
### Run unit test
- To run the unit test in file test.py you can run
  `pytest test.py`
- If you use PycharmIDE you can click on the green button like image below to excute the unit test 
- ![img.png](img.png)